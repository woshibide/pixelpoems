#include "ofApp.h"

// total number of possible color combinations for each pixel is
// 256 x 256 x 256 = 16,777,216

// Since there are 65,536 (256 x 256) pixels in a 256x256 image,
// the total number of possible pictures that the program can produce is:
// 16,777,216^(65,536) ≈ 4.316 x 10^1650528

// ----------

// 65,536 words are required for 256x256 image


//--------------------------------------------------------------
void ofApp::setup(){
    
    img.allocate(imgRes, imgRes, OF_IMAGE_COLOR);
    write.load("FragmentMono-Regular.ttf", 16);
    
    running = true;
    cycleCount = 0;
    
    ofBuffer poem = ofBufferFromFile("edgarAllanPoe.txt");
    inputPoem = poem.getText();
}

//--------------------------------------------------------------
void ofApp::update(){
    
    while (running){
        textToImage(inputPoem, img);
        break;
    }
        
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    ofSetColor(ofColor::black);
    
    if (ofGetFrameNum() % 10 == 0) {
        img.draw(0,0,ofGetWidth(),ofGetHeight());
    }
    
    guiDisplay();
    // would be great to output image or every 10th image, just so the process of work is visible
    // or maybe ascii fire burning https://play.ertdfgcvb.xyz/#/src/demos/doom_flame_full_color
    
}

void ofApp::guiDisplay(){
    
    write.drawString("spacebar to pause or play", 20, ofGetHeight() - 40);
    
    // fps
    string fpsStr = "fps: " + ofToString(ofGetFrameRate());
    write.drawString(fpsStr, 20, 40);
    
    // cycle
    write.drawString("cycle: " + cycleNum, 20, 60);
    // mute color on !running state
}

// ------ TEXT TO IMAGE ------

void ofApp::textToImage(const string& inputPoem, ofImage & image){
    
        // Split the input string into an array of words
        vector<string> wordsArray;
        istringstream iss(inputPoem);
        string word;
        while (iss >> word) {
            wordsArray.push_back(word);
        }
        
        // Shuffle the array of words
        random_device rd;
        mt19937 g(rd());
        shuffle(wordsArray.begin(), wordsArray.end(), g);
        
        // Save the shuffled words to a string
        ostringstream oss;
        for (const auto& word : wordsArray) {
            oss << word << " ";
        }
        string shuffledText = oss.str();
    
        // Save new poem
        pixelPoem.open("texts/txtPoem_" + ofToString(cycleCount) + ".txt", ofFile::WriteOnly);
        pixelPoem << shuffledText;
 
        // Draw the image
        for (int x = 0; x < imgRes; x++) {
            for (int y = 0; y < imgRes; y++) {
                int index = y * imgRes + x;
//                if (index >= wordsArray.size()) {
                int wordIndex = index % wordsArray.size();
                    string word = wordsArray[wordIndex];
                    int hue = ofMap(hash<string>{}(word),
                                    0, numeric_limits<size_t>::max(), 0, 255);
                    img.setColor(x, y, ofColor::fromHsb(hue, 255, 255));
//                } else {
//                    img.setColor(x, y, ofColor::black);
//                    // need to use 0,0,0 value as well
//                }
            }
        }
    img.update();

    // Begin saving the screen as an SVG
    ofBeginSaveScreenAsSVG("imagery/imgPoem_" + ofToString(cycleCount) + ".svg");

    // Draw the image
    img.draw(0, 0, imgRes, imgRes);

    // End saving the screen as an SVG
    ofEndSaveScreenAsSVG();

    
    
    cycleNum = ofToString(cycleCount++);
    //ofSleepMillis(50); // <---------------- overload switch works terrible
}

// ------ IMAGE TO TEXT ------

//void ofApp::imgToText(img){
//    // get image
//    // get each pixel value
//    // compare RGB against ASCII
//    // find median
//    // determine random diapason of colors to be interpreted as single word
//       x 256*256=65,536 times
//    // produce characters for each of the words
//    // fill those string arrays with char
//    // return txt file
//}





//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if(key == ' '){
            running = !running; // on and off switch
        }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
