#pragma once

#include "ofMain.h"
#include <stdio.h>
#include <sstream>
#include <vector>
#include <algorithm>
#include <random>
#include "ofxSVG.h"


using namespace std;


class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
    ofFile pixelPoem;
    ofImage img;
    ofxSVG svg;
    const int imgRes = 256;
    
    int cycleCount;
    string cycleNum;
    bool running;
    
    ofTrueTypeFont write;
    string inputPoem;
    
    
    void textToImage(const std::string& inputString, ofImage & image);
    void guiDisplay();
    
};
